using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
namespace AutoInvoicing
{
	/// <summary>
	/// Summary description for DBClass.
	/// </summary>
	public class DBClass
	{
        private static string QuoteSystemLocal = ConfigurationSettings.AppSettings["QuoteSystemLocal"];
        private string SysproLocal = ConfigurationSettings.AppSettings["SysproLocal"];

        public static string LocalDBServer = ConfigurationSettings.AppSettings["QLocalReportServer"];
        public static string LocalDBLogin = ConfigurationSettings.AppSettings["QLocalReportLogin"];
        public static string LocalDBPassword = ConfigurationSettings.AppSettings["QLocalReportPassword"];
        public static string LocalDB = ConfigurationSettings.AppSettings["QLocalReportDatabase"];

        public static string ELocalDBServer = ConfigurationSettings.AppSettings["ELocalReportServer"];
        public static string ELocalDBLogin = ConfigurationSettings.AppSettings["ELocalReportLogin"];
        public static string ELocalDBPassword = ConfigurationSettings.AppSettings["ELocalReportPassword"];
        public static string ELocalDB = ConfigurationSettings.AppSettings["ELocalReportDatabase"];

        private SqlConnection SqlCon;
        private SqlConnection SqlConSYS;

        private static readonly object m_Lock = new object();

        public SqlConnection GetConnection()
        {
            if (SqlCon == null || SqlCon.State == ConnectionState.Closed)
            {
                SqlCon = new SqlConnection();
                try
                {
                    SqlCon.ConnectionString = QuoteSystemLocal;
                }
                catch (Exception)
                {
                    if (SqlCon.State != ConnectionState.Closed)
                    {
                        SqlCon.Close();
                    }
                    SqlCon.Dispose();
                }
                lock (m_Lock)
                {
                    SqlCon.Open();
                }
            }

            return SqlCon;
        }
        public void Close()
        {
            if (SqlCon != null && SqlCon.State == ConnectionState.Open)
                SqlCon.Close();
        }
        public SqlConnection GetConnectionSYS()
        {
            if (SqlConSYS == null || SqlConSYS.State == ConnectionState.Closed)
            {
                SqlConSYS = new SqlConnection();
                try
                {
                    SqlConSYS.ConnectionString = SysproLocal;
                }
                catch (Exception)
                {
                    if (SqlConSYS.State != ConnectionState.Closed)
                    {
                        SqlConSYS.Close();
                    }
                    SqlConSYS.Dispose();
                }
                lock (m_Lock)
                {
                    SqlConSYS.Open();
                }
            }
            return SqlConSYS;
        }
        public void CloseSYS()
        {
            if (SqlConSYS != null)
                SqlConSYS.Close();
            SqlConSYS.Dispose();
        }
		public DataSet Select_Open_Invoice()
		{  		
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.Text;
			//migration start backup
//			command.CommandText= "select top 50 customer,[Name],Telephone,Fax,OverDueInvoice,CurrencyValue,InvoiceDate,CustomerPONumber,SalesOrder from Openquery( \"COWAN-NT-2003\",'select inv.customer,inv.CurrencyValue,inv.Invoice AS OverDueInvoice ,inp.Invoice AS Invoice ,inv.InvoiceDate,inv.CustomerPONumber,arc.Name,arc.Telephone,arc.fax,inv.SalesOrder  "
//				+" from ARINVOICE as inv left join ARINVOICEPAY as inp on inv.Invoice = inp.Invoice and inv.customer=inp.customer  left join ARCUSTOMER as arc on inv.customer=arc.customer where inv.DocumentType=\"I\" and CurrencyValue >=\"0\"  order by inv.INVOICE DESC') where Invoice is null group by customer,[Name],Telephone,Fax,OverDueInvoice,CurrencyValue,InvoiceDate,CustomerPONumber,SalesOrder;";
			//migration end backup
			//migration start update
			command.CommandText= "select top 50 customer,[Name],Telephone,Fax,OverDueInvoice,CurrencyValue,InvoiceDate,CustomerPONumber,SalesOrder from Openquery( \"lnkSYSPRONEW\",'select inv.Customer,inv.CurrencyValue,inv.Invoice AS OverDueInvoice ,inp.Invoice AS Invoice ,inv.InvoiceDate,inv.CustomerPoNumber,arc.Name,arc.Telephone,arc.Fax,inv.SalesOrder  "
				 +" from ArInvoice as inv left join ArInvoicePay as inp on inv.Invoice = inp.Invoice and inv.Customer=inp.Customer  left join ArCustomer as arc on inv.Customer=arc.Customer where inv.DocumentType=''I'' and CurrencyValue >=''0''  order by inv.Invoice DESC') where Invoice is null group by customer,[Name],Telephone,Fax,OverDueInvoice,CurrencyValue,InvoiceDate,CustomerPONumber,SalesOrder;";
			//migration end update
			DataSet ds=new DataSet();
			try
			{
				command.Connection = GetConnectionSYS();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;
				oledad.Fill(ds);
                CloseSYS();
			}
			finally
			{
                SqlConSYS.Close();
			}
			return ds;
		}
		public DataSet Select_Open_Invoice_ByInv(string invoice)
		{  		
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.Text;
			//migration start backup
//			command.CommandText= "select top 50 customer,[Name],Telephone,Fax,OverDueInvoice,CurrencyValue,InvoiceDate,CustomerPONumber,SalesOrder from Openquery( \"COWAN-NT-2003\",'select inv.customer,inv.CurrencyValue,inv.Invoice AS OverDueInvoice ,inp.Invoice AS Invoice ,inv.InvoiceDate,inv.CustomerPONumber,arc.Name,arc.Telephone,arc.fax,inv.SalesOrder  "
//				+" from ARINVOICE as inv left join ARINVOICEPAY as inp on inv.Invoice = inp.Invoice and inv.customer=inp.customer  left join ARCUSTOMER as arc on inv.customer=arc.customer where inv.DocumentType=\"I\" and inv.Invoice like \"%"+invoice.Trim()+"%\" and CurrencyValue >=\"0\"  order by inv.INVOICE ASC') group by customer,[Name],Telephone,Fax,OverDueInvoice,CurrencyValue,InvoiceDate,CustomerPONumber,SalesOrder;";
			//migration end backup
			//migration start update
			command.CommandText= "select top 50 customer,[Name],Telephone,Fax,OverDueInvoice,CurrencyValue,InvoiceDate,CustomerPONumber,SalesOrder from Openquery( \"lnkSYSPRONEW\",'select inv.Customer,inv.CurrencyValue,inv.Invoice AS OverDueInvoice ,inp.Invoice AS Invoice ,inv.InvoiceDate,inv.CustomerPoNumber,arc.Name,arc.Telephone,arc.Fax,inv.SalesOrder  "
				 +" from ArInvoice as inv left join ArInvoicePay as inp on inv.Invoice = inp.Invoice and inv.Customer=inp.Customer  left join ArCustomer as arc on inv.Customer=arc.Customer where inv.DocumentType=''I'' and inv.Invoice like ''%"+invoice.Trim()+"%'' and CurrencyValue >=''0''  order by inv.Invoice ASC') group by customer,[Name],Telephone,Fax,OverDueInvoice,CurrencyValue,InvoiceDate,CustomerPONumber,SalesOrder;";
			//migration end update
			DataSet ds=new DataSet();
			try
			{
                command.Connection = GetConnectionSYS();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;
				oledad.Fill(ds);
                CloseSYS();
			}
			finally
			{
                SqlConSYS.Close();
			}
			return ds;
		}
		public string Select_Invoiceemail(string code)
		{  
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.Text;
			command.CommandText= "select AREmails from Customers_TableV1 where Sysprocode='"+code.Trim()+"'";
			string str="";
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
				if(reader.Read())
				{
					str =(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return str;
		}
		public string Select_InvoiceStatus(string invoice)
		{  
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.Text;
			command.CommandText= "select Status from Invoice_Send_Table where Invoice='"+invoice.Trim()+"'";
			string str="";
			try
			{
                command.Connection = GetConnectionSYS();
				SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
				if(reader.Read())
				{
					str =(reader.GetValue(0).ToString());
				}
				reader.Close();
                CloseSYS();
			}
			finally
			{
                SqlConSYS.Close();
			}
			return str;
		}
		public string Insert_InvoiceStatus(string invoice,string invdate,string status)
		{  
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.Text;
			command.CommandText= "INSERT INTO [Invoice_Send_Table] ([Invoice],[InvoiceDate],[Status]) VALUES('"+invoice.Trim()+"','"+invdate.Trim()+"','"+status.Trim()+"')";
			string str="";
			try
			{
                command.Connection = GetConnectionSYS();
				str =command.ExecuteNonQuery().ToString();
                CloseSYS();
			}
			finally
			{
                SqlConSYS.Close();
			}
			return str;
		}
		public string FindSONO(string sono)
		{  		
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.Text;
			//migration start backup
			//command.CommandText= "select * from  openquery( \"COWAN-NT-2003\",'SELECT Distinct SalesOrder FROM SORMASTER where SalesOrder=\""+sono.Trim()+"\"')" ;
			//migration end backup
			//migration start update
			command.CommandText= "select * from  openquery( \"lnkSYSPRONEW\",'SELECT Distinct SalesOrder FROM SorMaster where SalesOrder=''"+sono.Trim()+"''')" ;
			//migration end update
			string str="";
			try
			{
                command.Connection = GetConnectionSYS();
				SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
				if(reader.Read())
				{
					str=((reader.GetValue(0)).ToString());
				}
				reader.Close();
                CloseSYS();
			}
			catch(SqlException ex)
			{
				string ees=ex.Message;
			}
			finally
			{
                SqlConSYS.Close();
			}
			return str;
		}
		public string FindSONO_SMC(string sono)
		{  		
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.Text;
			//migration start backup
//			command.CommandText= "select * from  openquery( \"COWAN-NT-2003\",'SELECT Distinct Customer FROM SORMASTER where SalesOrder=\""+sono.Trim()+"\"')" ;
			//migration end backup
			//migration start update
			command.CommandText= "select * from  openquery( \"lnkSYSPRONEW\",'SELECT Distinct Customer FROM SorMaster where SalesOrder=''"+sono.Trim()+"''')" ;
			//migration end update
			string str="";
			try
			{
                command.Connection = GetConnectionSYS();
				SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
				if(reader.Read())
				{
					str=((reader.GetValue(0)).ToString());
				}
				reader.Close();
                CloseSYS();
			}
			catch(SqlException ex)
			{
				string ees=ex.Message;
			}
			finally
			{
                SqlConSYS.Close();
			}
			return str;
		}
		public DBClass()
		{
		}
	}
}
