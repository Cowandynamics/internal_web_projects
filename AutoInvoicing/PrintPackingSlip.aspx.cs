using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace AutoInvoicing
{
	/// <summary>
	/// Summary description for PrintPackingSlip.
	/// </summary>
	public partial class PrintPackingSlip : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			unittest1();
			if(! IsPostBack)
			{
				TxtSalesorder.Text="";
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		public void unittest()
		{
			DBClass db=new DBClass();
			Lblinfo.Text="";
			string sono="";
			sono=db.FindSONO("191264");
			if(sono.Trim() !="")
			{
				CrystalDecisions.CrystalReports.Engine.ReportDocument objR= new CrystalDecisions.CrystalReports.Engine.ReportDocument();
				string path;
				path = Request.PhysicalApplicationPath;
				string smc="";
				smc=db.FindSONO_SMC("191264");
				if(smc.Trim().Substring(0,3) =="SMC")
				{
					objR.Load(path +"\\crtfiles\\PackingSlip_ReportSO.rpt");
				}
				else
				{
					objR.Load(path +"\\crtfiles\\PackingSlip_ReportSO_Other.rpt");
				}												
				objR.Refresh();       
				objR.SetDatabaseLogon("sa","37YggDDj");
				objR.SetParameterValue("@sono","191264");						
				CrystalDecisions.Shared.DiskFileDestinationOptions diskOpts = new CrystalDecisions.Shared.DiskFileDestinationOptions();
				objR.ExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.Excel;
				objR.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
				diskOpts.DiskFileName = path+"/invoice/packingslip_"+sono.ToString().Trim()+".xls";
				objR.ExportOptions.DestinationOptions = diskOpts;
				objR.Export();
				Lblinfo.Text="xls file is created";
				LblLink.Text="<A href='invoice/packingslip_"+sono.ToString()+".xls' target='_blank'>Click here to Dowload</A>";					}
		}
		protected void LBPrint_Click(object sender, System.EventArgs e)
		{
			
			try
			{
				if(TxtSalesorder.Text.Trim() !="")
				{
					DBClass db=new DBClass();
					Lblinfo.Text="";
					string sono="";
					sono=db.FindSONO(TxtSalesorder.Text.Trim());
					if(sono.Trim() !="")
					{
						CrystalDecisions.CrystalReports.Engine.ReportDocument objR= new CrystalDecisions.CrystalReports.Engine.ReportDocument();
						string path;
						path = Request.PhysicalApplicationPath;
						string smc="";
						smc=db.FindSONO_SMC(TxtSalesorder.Text.Trim());
						if(smc.Trim().Substring(0,3) =="SMC")
						{
							objR.Load(path +"\\crtfiles\\PackingSlip_ReportSO.rpt");
						}
						else
						{
							objR.Load(path +"\\crtfiles\\PackingSlip_ReportSO_Other.rpt");
						}												
						objR.Refresh();       
						objR.SetDatabaseLogon("sa","37YggDDj");
						objR.SetParameterValue("@sono",TxtSalesorder.Text.ToString().Trim());						
						CrystalDecisions.Shared.DiskFileDestinationOptions diskOpts = new CrystalDecisions.Shared.DiskFileDestinationOptions();
						objR.ExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.Excel;
						objR.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
						diskOpts.DiskFileName = path+"/invoice/packingslip_"+TxtSalesorder.Text.ToString().Trim()+".xls";
						objR.ExportOptions.DestinationOptions = diskOpts;
						objR.Export();
						Lblinfo.Text="xls file is created";
						LblLink.Text="<A href='invoice/packingslip_"+sono.ToString()+".xls' target='_blank'>Click here to Dowload</A>";					}
					else
					{
						Lblinfo.Text=("No Items");
					}					
				}
				else
				{
					Lblinfo.Text=("Enter a S/O No.");
				}
			}
			catch(Exception ex)
			{
				Lblinfo.Text=ex.Message;
			}
		}

		public void unittest1()
		{
			DBClass db=new DBClass();
			Lblinfo.Text="";
			string sono="";
			sono=db.FindSONO("185246");
			if(sono.Trim() !="")
			{
				CrystalDecisions.CrystalReports.Engine.ReportDocument objR= new CrystalDecisions.CrystalReports.Engine.ReportDocument();
				string path;
				path = Request.PhysicalApplicationPath;
				string smc="";
				smc=db.FindSONO_SMC("185246");
				if(smc.Trim().Substring(0,3) =="SMC")
				{
					objR.Load(path +"\\crtfiles\\PackingSlip_ReportSO.rpt");
				}
				else
				{
					objR.Load(path +"\\crtfiles\\PackingSlip_ReportSO_Other.rpt");
				}												
				objR.Refresh();       
				objR.SetDatabaseLogon("sa","37YggDDj");
				objR.SetParameterValue("@sono","185246");						
				CrystalDecisions.Shared.DiskFileDestinationOptions diskOpts = new CrystalDecisions.Shared.DiskFileDestinationOptions();
				objR.ExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
				objR.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
				diskOpts.DiskFileName = path+"/invoice/packingslip_"+sono.ToString().Trim()+".pdf";
				objR.ExportOptions.DestinationOptions = diskOpts;
				objR.Export();
				Lblinfo.Text="pdf file is created";
				LblLink.Text="<A href='invoice/packingslip_"+sono.ToString()+".pdf' target='_blank'>Click here to Dowload</A>";	
			}
		}
		protected void LBPrintPDF_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(TxtSalesorder.Text.Trim() !="")
				{
					DBClass db=new DBClass();
					Lblinfo.Text="";
					string sono="";
					sono=db.FindSONO(TxtSalesorder.Text.Trim());
					if(sono.Trim() !="")
					{
						CrystalDecisions.CrystalReports.Engine.ReportDocument objR= new CrystalDecisions.CrystalReports.Engine.ReportDocument();
						string path;
						path = Request.PhysicalApplicationPath;
						string smc="";
						smc=db.FindSONO_SMC(TxtSalesorder.Text.Trim());
						if(smc.Trim().Substring(0,3) =="SMC")
						{
							objR.Load(path +"\\crtfiles\\PackingSlip_ReportSO.rpt");
						}
						else
						{
							objR.Load(path +"\\crtfiles\\PackingSlip_ReportSO_Other.rpt");
						}												
						objR.Refresh();       
						objR.SetDatabaseLogon("sa","37YggDDj");
						objR.SetParameterValue("@sono",TxtSalesorder.Text.ToString().Trim());						
						CrystalDecisions.Shared.DiskFileDestinationOptions diskOpts = new CrystalDecisions.Shared.DiskFileDestinationOptions();
						objR.ExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
						objR.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
						diskOpts.DiskFileName = path+"/invoice/packingslip_"+TxtSalesorder.Text.ToString().Trim()+".pdf";
						objR.ExportOptions.DestinationOptions = diskOpts;
						objR.Export();
						Lblinfo.Text="pdf file is created";
						LblLink.Text="<A href='invoice/packingslip_"+sono.ToString()+".pdf' target='_blank'>Click here to Dowload</A>";					}
					else
					{
						Lblinfo.Text=("No Items");
					}					
				}
				else
				{
					Lblinfo.Text=("Enter a S/O No.");
				}
			}
			catch(Exception ex)
			{
				Lblinfo.Text=ex.Message;
			}
		}
	}
}
