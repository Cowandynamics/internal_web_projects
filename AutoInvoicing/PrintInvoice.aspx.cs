using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.IO;
namespace AutoInvoicing
{
	/// <summary>
	/// Summary description for PrintInvoice.
	/// </summary>
	public partial class PrintInvoice : System.Web.UI.Page
	{
		DBClass db=new DBClass();
		
		public void unittest()
		{
			string path;
			path = Request.PhysicalApplicationPath;
			RemoveFiles( path+"/invoice/");
			CrystalDecisions.CrystalReports.Engine.ReportDocument objReport = new CrystalDecisions.CrystalReports.Engine.ReportDocument();					
			objReport.Load(path +"crtfiles/Invoice_Report_Template2012.rpt");
			objReport.Refresh();       
			objReport.SetDatabaseLogon("sa","37YggDDj");
			objReport.SetParameterValue("@invdate", "2012/10/10");
			objReport.SetParameterValue("@sysprocode", "XSTCUIV");
			CrystalDecisions.Shared.DiskFileDestinationOptions diskOpts = new CrystalDecisions.Shared.DiskFileDestinationOptions();
			objReport.ExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
			objReport.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
			diskOpts.DiskFileName = path+"/invoice/Invoice_037004.pdf";
			objReport.ExportOptions.DestinationOptions = diskOpts;
			objReport.Export();
			Lblview.Text="<iframe id='NewsWindow' name='fframe' src="+"invoice/Invoice_037004.pdf"+" width='750' height='900' marginwidth='0' marginheight='0' frameborder='0' scrolling='auto' ></iframe>";
					
		}
		protected void Page_Load(object sender, System.EventArgs e)
		{
			//unittest();
			try
			{
				lbl.Text="";
				if(! IsPostBack)
				{
					if(Request.QueryString["invoice"] !=null && Request.QueryString["customercode"] !=null)
					{
						string radm=DateTime.Now.Ticks.ToString();
						string invoice=Request.QueryString["invoice"].Trim();
						//migration start update
						string invdate=Request.QueryString["invdate"].Trim();
						string customercode=Request.QueryString["customercode"].Trim();
						//migration end update
						string status="";
						status=db.Select_InvoiceStatus(invoice.Trim());
						if(status.Trim() =="1")
						{
							LBEmail.Visible=false;
							LBFax.Visible=false;
							LBNOtNow.Visible=false;
							lblq.Visible=false;
							lblstatus.Visible=true;
						}
						else 
						{
							LBEmail.Visible=true;
							LBFax.Visible=true;
							LBNOtNow.Visible=true;
							lblq.Visible=true;
							lblstatus.Visible=false;
						}
						string path;
						path = Request.PhysicalApplicationPath;
						RemoveFiles( path+"/invoice/");
						CrystalDecisions.CrystalReports.Engine.ReportDocument objReport = new CrystalDecisions.CrystalReports.Engine.ReportDocument();					
						//migration start backup
//						objReport.Load(path +"crtfiles/Invoice_Report_Template.rpt");
//						objReport.Refresh();       
//						objReport.SetDatabaseLogon("sa","37YggDDj");
//						objReport.SetParameterValue("@invoice",invoice.Trim());
						//migration end backup
						//migration start update
						objReport.Load(path +"crtfiles/Invoice_Report_Template2012.rpt");
						objReport.Refresh();       
						objReport.SetDatabaseLogon("sa","37YggDDj");
						// issue #78
//                        objReport.SetParameterValue("@invdate", invdate);
//						objReport.SetParameterValue("@sysprocode", customercode);
						objReport.SetParameterValue("@invoice",invoice.Trim());
						//migration end update
						CrystalDecisions.Shared.DiskFileDestinationOptions diskOpts = new CrystalDecisions.Shared.DiskFileDestinationOptions();
						objReport.ExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
						objReport.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
						diskOpts.DiskFileName = path+"/invoice/Invoice_"+invoice.Trim()+radm.ToString()+".pdf";
						objReport.ExportOptions.DestinationOptions = diskOpts;
						objReport.Export();
						Lblview.Text="<iframe id='NewsWindow' name='fframe' src="+"invoice/Invoice_"+invoice.Trim()+radm.ToString()+".pdf"+" width='750' height='900' marginwidth='0' marginheight='0' frameborder='0' scrolling='auto' ></iframe>";
					
					}
				}
			}
			catch (Exception ex)
			{
				Lblview.Text=(ex.Message.ToString());
			}
		}
		public void RemoveFiles(string strPath)
		{			 
			System.IO.DirectoryInfo di = new DirectoryInfo(strPath);
			FileInfo[] fiArr = di.GetFiles();
			foreach (FileInfo fri in fiArr)
			{
				if(fri.Extension.ToString() ==".pdf")
				{
					fri.Delete();
				}
			}
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void LBEmail_Click(object sender, System.EventArgs e)
		{
			if(Request.QueryString["invoice"] !=null && Request.QueryString["customercode"] !=null)
			{
				string radm=DateTime.Now.Ticks.ToString();
				string invoice=Request.QueryString["invoice"].Trim();
				string customercode=Request.QueryString["customercode"].Trim();
				string invdate=Request.QueryString["invdate"].Trim();
				try
				{
					string path;
					path = Request.PhysicalApplicationPath;
					RemoveFiles( path+"/invoice/");
					CrystalDecisions.CrystalReports.Engine.ReportDocument objReport = new CrystalDecisions.CrystalReports.Engine.ReportDocument();					
					//migration start backup
//					objReport.Load(path +"crtfiles/Invoice_Report_Template.rpt");
//					objReport.Refresh();       
//					objReport.SetDatabaseLogon("sa","37YggDDj");
//					objReport.SetParameterValue("@invoice",invoice.Trim());
					//migration end backup
					//migration start update
					objReport.Load(path +"crtfiles/Invoice_Report_Template2012.rpt");
					objReport.Refresh();       
					objReport.SetDatabaseLogon("sa","37YggDDj");
					//issue #96 start
					//backup
//					objReport.SetParameterValue("@invdate", invdate);
//					objReport.SetParameterValue("@sysprocode", customercode);
					//update
					objReport.SetParameterValue("@invoice",invoice.Trim());
					//migration end update
					CrystalDecisions.Shared.DiskFileDestinationOptions diskOpts = new CrystalDecisions.Shared.DiskFileDestinationOptions();
					objReport.ExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
					objReport.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
					diskOpts.DiskFileName = path+"/invoice/Invoice_"+invoice.Trim()+radm.ToString()+".pdf";
					objReport.ExportOptions.DestinationOptions = diskOpts;
					objReport.Export();
                    Lblview.Text = "<iframe id='NewsWindow' name='fframe' src=" + "invoice/Invoice_" + invoice.Trim() + radm.ToString() + ".pdf" + " width='750' height='900' marginwidth='0' marginheight='0' frameborder='0' scrolling='auto' ></iframe>";

					string to="";
                    string cc = "";
					to = db.Select_Invoiceemail(customercode.ToString().Trim());
                    if (to.Trim() == "")
                    {
                        to = "danny@cowandynamics.com";
                    }
                    else 
                    {
                        cc = "danny@cowandynamics.com";
                    } 
                    MailService.SendMail(
                        "danny@cowandynamics.com",
                        to,
                        cc,
                        "Invoice :" + invoice.ToString().Trim(),
                        "see attached" + MailService.Get_Admin(),
                        path + "/invoice/Invoice_" + invoice.Trim() + radm.ToString() + ".pdf",
                        false
                        );			
		
					db.Insert_InvoiceStatus(invoice.ToString().Trim(),DateTime.Today.ToShortDateString(),"1");
					lbl.Text="An email is send to the customer";
				}
				catch (Exception ex)
				{
					Lblview.Text=(ex.Message.ToString());
				}
			}
		}

		protected void LBFax_Click(object sender, System.EventArgs e)
		{	
				if(Request.QueryString["invoice"] !=null && Request.QueryString["customercode"] !=null)
				{
					string radm=DateTime.Now.Ticks.ToString();
					string invoice=Request.QueryString["invoice"].Trim();
					string customercode=Request.QueryString["customercode"].Trim();
					string invdate=Request.QueryString["invdate"].Trim();
					try
					{
						string path;
						path = Request.PhysicalApplicationPath;
						RemoveFiles( path+"/invoice/");
						CrystalDecisions.CrystalReports.Engine.ReportDocument objReport = new CrystalDecisions.CrystalReports.Engine.ReportDocument();					
						//migration start backup
//						objReport.Load(path +"crtfiles/Invoice_Report_Template.rpt");
//						objReport.Refresh();       
//						objReport.SetDatabaseLogon("sa","37YggDDj");
//						objReport.SetParameterValue("@invoice",invoice.Trim());
						//migration end backup
						//migration start update
						objReport.Load(path +"crtfiles/Invoice_Report_Template2012.rpt");
						objReport.Refresh();       
						objReport.SetDatabaseLogon("sa","37YggDDj");
						objReport.SetParameterValue("@invdate", invdate);
						objReport.SetParameterValue("@sysprocode", customercode);
						//migration end update
						CrystalDecisions.Shared.DiskFileDestinationOptions diskOpts = new CrystalDecisions.Shared.DiskFileDestinationOptions();
						objReport.ExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
						objReport.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
						diskOpts.DiskFileName = path+"/invoice/Invoice_"+invoice.Trim()+radm.ToString()+".pdf";
						objReport.ExportOptions.DestinationOptions = diskOpts;
						objReport.Export();
						db.Insert_InvoiceStatus(invoice.ToString().Trim(),DateTime.Today.ToShortDateString(),"1");
						//migration start backup
						//Response.Redirect( path+"/invoice/Invoice_"+invoice.Trim()+".pdf");
						//migration end backup
						//migration start update
						Response.Redirect( path+"/invoice/Invoice_"+invoice.Trim()+radm.ToString()+".pdf");
						//migration end update
					}
					catch (Exception ex)
					{
						Lblview.Text=(ex.Message.ToString());
					}
				}
		}

		protected void LBNOtNow_Click(object sender, System.EventArgs e)
		{
			Lblview.Text ="<script language='javascript'> setTimeout('self.close();',1) </script>";
		}

		protected void lbEmailManual_Click(object sender, System.EventArgs e)
		{
			if(Request.QueryString["invoice"] !=null && Request.QueryString["customercode"] !=null && Page.IsValid)
			{
				string radm=DateTime.Now.Ticks.ToString();
				string invoice=Request.QueryString["invoice"].Trim();
				string customercode=Request.QueryString["customercode"].Trim();
				string invdate=Request.QueryString["invdate"].Trim();
				try
				{
					string path;
					path = Request.PhysicalApplicationPath;
					RemoveFiles( path+"/invoice/");
					CrystalDecisions.CrystalReports.Engine.ReportDocument objReport = new CrystalDecisions.CrystalReports.Engine.ReportDocument();					
					//migration start backup
					//					objReport.Load(path +"crtfiles/Invoice_Report_Template.rpt");
					//					objReport.Refresh();       
					//					objReport.SetDatabaseLogon("sa","37YggDDj");
					//					objReport.SetParameterValue("@invoice",invoice.Trim());
					//migration end backup
					//migration start update
					objReport.Load(path +"crtfiles/Invoice_Report_Template2012.rpt");
					objReport.Refresh();       
					objReport.SetDatabaseLogon("sa","37YggDDj");
					//issue #96 start
					//backup
					//					objReport.SetParameterValue("@invdate", invdate);
					//					objReport.SetParameterValue("@sysprocode", customercode);
					//update
					objReport.SetParameterValue("@invoice",invoice.Trim());
					//migration end update
					CrystalDecisions.Shared.DiskFileDestinationOptions diskOpts = new CrystalDecisions.Shared.DiskFileDestinationOptions();
					objReport.ExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
					objReport.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
					diskOpts.DiskFileName = path+"/invoice/Invoice_"+invoice.Trim()+radm.ToString()+".pdf";
					objReport.ExportOptions.DestinationOptions = diskOpts;
					objReport.Export();

                    Lblview.Text = "<iframe id='NewsWindow' name='fframe' src=" + "invoice/Invoice_" + invoice.Trim() + radm.ToString() + ".pdf" + " width='750' height='900' marginwidth='0' marginheight='0' frameborder='0' scrolling='auto' ></iframe>";

                    string to = "";
                    string cc = "";
                    if (txtEmailManual.Text.Trim() != "")
                    {
                        to = txtEmailManual.Text.TrimEnd();
                        cc = "mariline.g@cowandynamics.com";
                    }
                    else
                    {
                        to = "mariline.g@cowandynamics.com";
                    }
                    MailService.SendMail(
                        "mariline.g@cowandynamics.com",
                        to,
                        cc,
                        "Invoice :" + invoice.ToString().Trim(),
                        "see attached" + MailService.Get_Admin(),
                        path + "/invoice/Invoice_" + invoice.Trim() + radm.ToString() + ".pdf",
                        false
                        );	
				
					db.Insert_InvoiceStatus(invoice.ToString().Trim(),DateTime.Today.ToShortDateString(),"1");
					//test end
					lbl.Text="An email is send to the customer";
				}
				catch (Exception ex)
				{
					Lblview.Text=(ex.Message.ToString());
				}
			}
		}
	}
}
