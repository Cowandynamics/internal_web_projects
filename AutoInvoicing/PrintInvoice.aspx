<%@ Page language="c#" Codebehind="PrintInvoice.aspx.cs" AutoEventWireup="True" Inherits="AutoInvoicing.PrintInvoice" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PrintInvoice</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" rightMargin="0" topMargin="0" bgColor="#ffe4b5">
		<form id="Form1" method="post" runat="server">
			<TABLE style="Z-INDEX: 0" id="Table2" border="1" cellSpacing="1" borderColor="gray" cellPadding="1"
				width="300" align="center">
				<TR>
					<TD align="center"><asp:panel id="PanelStatus" runat="server">
							<TABLE style="WIDTH: 336px; HEIGHT: 67px" id="Table1" border="0" cellSpacing="1" cellPadding="1"
								width="336">
								<TR>
									<TD colSpan="7" align="center">
										<asp:Label id="lblq" runat="server" ForeColor="Red">Do you want to send this Invoice now?</asp:Label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:LinkButton id="LBEmail" runat="server" CausesValidation="False" Width="139px" Font-Bold="True" onclick="LBEmail_Click">Send By Email</asp:LinkButton></TD>
									<TD style="HEIGHT: 20px" align="center">
										<asp:LinkButton id="LBFax" runat="server" CausesValidation="False" Width="139px" Font-Bold="True" onclick="LBFax_Click">Send By Fax</asp:LinkButton></TD>
									<TD style="HEIGHT: 20px" align="center">
										<asp:LinkButton id="LBNOtNow" runat="server" CausesValidation="False" Width="139px" Font-Bold="True" onclick="LBNOtNow_Click">Not Now</asp:LinkButton></TD>
									<TD style="HEIGHT: 20px" align="center">
										<asp:LinkButton id="lbEmailManual" runat="server" Width="139px" Font-Bold="True" onclick="lbEmailManual_Click">Email Manually</asp:LinkButton></TD>
									<TD style="HEIGHT: 20px" align="center">
										<asp:TextBox id="txtEmailManual" runat="server"></asp:TextBox></TD>
									<TD style="HEIGHT: 20px" align="center">
										<asp:RequiredFieldValidator id="valrqEmailManual" runat="server" ControlToValidate="txtEmailManual" Height="20px"
											ErrorMessage="*"></asp:RequiredFieldValidator></TD>
									<TD style="HEIGHT: 20px" align="center">
										<asp:RegularExpressionValidator id="valrgEmailManual" runat="server" ControlToValidate="txtEmailManual" ErrorMessage="*"
											ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:label id="lblstatus" runat="server" ForeColor="Red" Visible="False">This Invoice is already send to the Customer</asp:label></TD>
				</TR>
				<TR>
					<TD height="100%" align="center">
						<asp:label style="Z-INDEX: 0" id="lbl" runat="server" ForeColor="Red"></asp:label></TD>
				</TR>
				<TR>
					<TD height="100%" align="center"><asp:label id="Lblview" runat="server"></asp:label></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
