using System;
using System.Net.Mail;
using System.IO;
using System.Configuration;
using System.Collections;

namespace AutoInvoicing
{
	public class MailService
	{
        public static string SmtpServer = ConfigurationSettings.AppSettings["SMTPServer"];
        public static string SmtpPort = ConfigurationSettings.AppSettings["SMTPPort"];
        public static string SmtpLogin = ConfigurationSettings.AppSettings["SMTPlogin"];
        public static string SmtpPassword = ConfigurationSettings.AppSettings["SMTPPassword"];
        public static bool SmtpDebug = bool.Parse(ConfigurationSettings.AppSettings["SMTPDebug"]);
        public static string Root = ConfigurationSettings.AppSettings["QRoot"];

        private MailService()
        { }
        public static void SendMail(string from, string to, string cc, string subject, string body, string attachmentPath, bool highPriority)
        {
            MailMessage message = new MailMessage();
            message.Sender = new MailAddress("admin@cowandynamics.com", "[Cowandynamics]");
            message.From = new MailAddress("admin@cowandynamics.com","[Cowandynamics]");
            if (!string.IsNullOrEmpty(from))
            {
                message.Headers.Add("Disposition-Notification-To", "<" + from + ">");
            }
            string invalidEmailText = "";
            string[] emails = to.IndexOf(',') > 0 ?  to.Split(';') : (to.IndexOf(';') > 0 ? to.Split(';') : to.Split(' '));
            if (emails.Length > 0)
            {
                foreach (string email in emails)
                {
                    if (!string.IsNullOrEmpty(email.Trim()) && IsValidEmail(email.Trim()))
                    {
                        message.To.Add(new MailAddress(email.Trim()));
                    }
                    else
                    {
                        message.To.Add(new MailAddress("mariline.g@cowandynamics.com"));
                        invalidEmailText = "Invoice couldn't email to the AR email specifed<br><p>Please correct the email address(Separate emails using ';' or ','):" + to + "<p><br><br>";
                    }
                }
            }
            else 
            {
                message.To.Add(new MailAddress("admin@cowandynamics.com"));
            }
            if (!string.IsNullOrEmpty(cc)&& IsValidEmail(cc.Trim()))
            {
                message.CC.Add(new MailAddress(cc.Trim()));
            }

            if (SmtpDebug)
            {
                message.Bcc.Add(new MailAddress("admin@cowandynamics.com"));
            }
            message.Subject = subject;
            message.Body = invalidEmailText + body;
            message.IsBodyHtml = true;

            if (highPriority)
            {
                message.Priority = MailPriority.High;
            }

            SmtpClient client = new SmtpClient(SmtpServer, int.Parse(SmtpPort));
            client.EnableSsl = true;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(SmtpLogin, SmtpPassword);

            if (!string.IsNullOrEmpty(attachmentPath))
            {
                Attachment attach = new Attachment(attachmentPath);
                message.Attachments.Add(attach);
            }
            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
                StreamWriter wrtr = new StreamWriter(Root + "/Auto Invoicing.txt", true);
                wrtr.WriteLine(DateTime.Now.ToString() + " | " + ex.Message + "/r/n");
                wrtr.Close();
            }
        }

        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
		public static  string Get_Admin()
		{
			string strSignature = @"<br />"
				+ "<br />"
				+ "<br />Danny Cotton"
				+ "<br />Service a la clientele, Customer service"
				+ "<br />"
				+ "<br />Cowan Dynamics Inc."
				+ "<br />6194 Notre-Dame Ouest"
				+ "<br />Montreal, Quebec H4C 1V4"
				+ "<br />tel : 514-341-3415 x300"
				+ "<br />Fax : 514-341-0249"
				+ "<br />"
				+ "<br />e-mail : danny@cowandynamics.com"
				+ "<br />Web: www.cowandynamics.com";
				return strSignature;
		}
		public static  string Get_DWG()
		{
			string strSignature = @"<br />"
				+ "<br />"
				+ "<br />Azita Malek, Eng. M.Sc."
				+ "<br />Director, Engineering and R&D"
				+ "<br />"
				+ "<br />Cowan Dynamics Inc."
				+ "<br />6194 Notre-Dame Ouest"
				+ "<br />Montreal, Quebec H4C 1V4"
				+ "<br />tel : 514-341-3415 x323"
				+ "<br />Fax : 514-341-0249"
				+ "<br />"
				+ "<br />e-mail : amalek@cowandynamics.com"
				+ "<br />Web: www.cowandynamics.com";
				return strSignature;
		}
		public static  string Get_Dorin()
		{
			string strSignature = @"<br />"
				+ "<br />"
				+ "<br />Dorin Taranu"
				+ "<br />Project Manager Process Automation"
				+ "<br />"
				+ "<br />Cowan Dynamics Inc."
				+ "<br />6194 Notre-Dame Ouest"
				+ "<br />Montreal, Quebec H4C 1V4"
				+ "<br />tel : 514-341-3415 x302"
				+ "<br />Fax : 514-341-0249"
				+ "<br />"
				+ "<br />e-mail : dtaranu@cowandynamics.com"
				+ "<br />Web: www.cowandynamics.com";
			return strSignature;
		}
		public static  string Get_Jenny()
		{
			string strSignature = @"<br />"
				+ "<br />"
				+ "<br />Jenny Luo, P.Eng."
				+ "<br />Application Engineer"
				+ "<br />"
				+ "<br />Cowan Dynamics Inc."
				+ "<br />6194 Notre-Dame Ouest"
				+ "<br />Montreal, Quebec H4C 1V4"
				+ "<br />tel : 514-341-3415 x322"
				+ "<br />Fax : 514-341-0249"
				+ "<br />"
				+ "<br />e-mail : jenny.l@cowandynamics.com"
				+ "<br />Web: www.cowandynamics.com";
			return strSignature;
		}
		public static  string Get_Jean()
		{
			string strSignature = @"<br />"
				+ "<br />"
				+ "<br />Jean Behara P.Eng."
				+ "<br />VP, Business Development"
				+ "<br />"
				+ "<br />Cowan Dynamics Inc."
				+ "<br />211 Watline Ave, S205"
				+ "<br />Mississauga, Ontario, L4Z 1P3"
				+ "<br />P: 905-829-2910 x201"
				+ "<br />C: 905-208-0985"
				+ "<br />"
				+ "<br />e-mail : jbehara@cowandynamics.com"
				+ "<br />Web: www.cowandynamics.com";
			return strSignature;
		}
		public static  string Get_Stephane()
		{
			string strSignature = @"<br />"
				+ "<br />"
				+ "<br />Stephane Meunier, Eng., PMP"
				+ "<br />Director, International Business Development"
				+ "<br />"
				+ "<br />Cowan Dynamics Inc."
				+ "<br />6194 Notre-Dame Ouest"
				+ "<br />Montreal, Quebec H4C 1V4"
				+ "<br />tel : 514-341-3415 x305"
				+ "<br />Fax : 514-341-0249"
				+ "<br />"
				+ "<br />e-mail : smeunier@cowandynamics.com"
				+ "<br />Web: www.cowandynamics.com";
			return strSignature;
		}
		public static string Get_Agent(string email)
		{
			string strEmail= "admin@cowandynamics.com";
			switch(email)
			{
				case "dtaranu@cowandynamics.com":
					strEmail = Get_Dorin();
					break;
				case "jenny.l@cowandynamics.com":
					strEmail = Get_Jenny();
					break;
				case "jbehara@cowandynamics.com":
					strEmail=Get_Jean();
					break;
				case "smeunier@cowandynamics.com":
					strEmail = Get_Stephane();
					break;
				default:
					strEmail= Get_Admin();
					break;
			}
			return strEmail;
		}
	}
}
