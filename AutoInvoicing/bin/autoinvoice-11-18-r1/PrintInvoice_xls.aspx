<%@ Register TagPrefix="cr" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=9.1.5000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>
<%@ Page language="c#" Codebehind="PrintInvoice_xls.aspx.cs" AutoEventWireup="false" Inherits="AutoInvoicing.PrintInvoice_xls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PrintInvoice_xls</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body bgColor="#ffe4b5">
		<form id="Form1" method="post" runat="server">
			<TABLE style="Z-INDEX: 0" id="Table1" border="1" cellSpacing="1" borderColor="silver" cellPadding="1"
				width="300">
				<TR>
					<TD style="HEIGHT: 24px" colSpan="2" align="center">
						<asp:label style="Z-INDEX: 0" id="LblPartNo" runat="server" Font-Underline="True" Font-Bold="True"
							Font-Size="9pt">Download Invoice in Excel format</asp:label></TD>
				</TR>
				<TR>
					<TD colSpan="2" align="center">
						<asp:label style="Z-INDEX: 0" id="LblLink" runat="server" Font-Size="10pt"></asp:label></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
