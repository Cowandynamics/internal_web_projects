<%@ Page language="c#" Codebehind="PrintPackingSlip.aspx.cs" AutoEventWireup="True" Inherits="AutoInvoicing.PrintPackingSlip" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PrintPackingSlip</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body bgColor="#ffe4b5">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" border="1" cellSpacing="1" borderColor="silver" cellPadding="1" width="376"
				style="WIDTH: 376px">
				<TR>
					<TD style="HEIGHT: 24px" colSpan="2" align="center">
						<asp:label style="Z-INDEX: 0" id="LblPartNo" runat="server" Font-Size="9pt" Font-Bold="True"
							Font-Underline="True">Search & Export Packing Slip</asp:label></TD>
				</TR>
				<TR>
					<TD align="right" width="50%">
						<asp:label style="Z-INDEX: 0" id="Label3" runat="server" Font-Size="10pt">Sales Order:</asp:label></TD>
					<TD align="left">
						<asp:textbox style="Z-INDEX: 0" id="TxtSalesorder" runat="server" Width="78px"></asp:textbox></TD>
				</TR>
				<TR>
					<TD colSpan="2" align="center">
						<asp:linkbutton style="Z-INDEX: 0" id="LBPrint" runat="server" Font-Size="9pt" Font-Bold="True"
							Width="140px" onclick="LBPrint_Click">Print Packing Slip (Excel)</asp:linkbutton>&nbsp;&nbsp;&nbsp;
						<asp:linkbutton style="Z-INDEX: 0" id="LBPrintPDF" runat="server" Font-Bold="True" Font-Size="9pt"
							Width="140px" onclick="LBPrintPDF_Click">Print Packing Slip (PDF)</asp:linkbutton></TD>
				</TR>
				<TR>
					<TD colSpan="2" align="center">
						<asp:label style="Z-INDEX: 0" id="Lblinfo" runat="server" Font-Size="10pt" ForeColor="Red"></asp:label></TD>
				</TR>
				<TR>
					<TD colSpan="2" align="center">
						<asp:label style="Z-INDEX: 0" id="LblLink" runat="server" Font-Size="10pt"></asp:label></TD>
				</TR>
			</TABLE>
			&nbsp;
		</form>
	</body>
</HTML>
