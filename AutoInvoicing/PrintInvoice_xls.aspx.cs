using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace AutoInvoicing
{
	/// <summary>
	/// Summary description for PrintInvoice_xls.
	/// </summary>
	public partial class PrintInvoice_xls : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(! IsPostBack)
			{
				if(Request.QueryString["invoice"] !=null)
				{
					string invoice=Request.QueryString["invoice"].Trim();					
					string path = Request.PhysicalApplicationPath;
					LblLink.Text="";
					CrystalDecisions.CrystalReports.Engine.ReportDocument objReport = new CrystalDecisions.CrystalReports.Engine.ReportDocument();					
					//migration start backup
					//objReport.Load(path +"crtfiles/Invoice_Report_Template.rpt");
					//migration end backup
					//migration start update
					string invdate=Request.QueryString["invdate"].Trim();
					string customercode=Request.QueryString["customercode"].Trim();
					objReport.Load(path +"crtfiles/Invoice_Report_Template2012.rpt");
					//migration end update
					objReport.Refresh();       
					objReport.SetDatabaseLogon("sa","37YggDDj");
					//migration start backup
					//issue parameter start backup
//					objReport.SetParameterValue("@invoice2",invoice.ToString());
//					objReport.SetParameterValue("@invoice3",invoice.ToString());
					//issue parameter end backup
					//issue parameter start update
					//objReport.SetParameterValue("@invoice",invoice.ToString());
					//issue parameter end update
					//migration start update
					// issue #78
//					objReport.SetParameterValue("@invdate", invdate);
//					objReport.SetParameterValue("@sysprocode", customercode);
                    objReport.SetParameterValue("@invoice",invoice.ToString());
					//migration end update
					CrystalDecisions.Shared.DiskFileDestinationOptions diskOpts = new CrystalDecisions.Shared.DiskFileDestinationOptions();
					objReport.ExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.Excel;
					objReport.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
					diskOpts.DiskFileName = path+"/invoice/Invoice_"+invoice.ToString()+".xls";
					objReport.ExportOptions.DestinationOptions = diskOpts;
					objReport.Export();
					LblLink.Text="<A href='invoice/Invoice_"+invoice.ToString()+".xls' tatget='_blank'>Click here to Dowload</A>";
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
