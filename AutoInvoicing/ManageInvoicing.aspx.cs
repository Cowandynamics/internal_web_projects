using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
namespace AutoInvoicing
{
	/// <summary>
	/// Summary description for WebForm1.
	/// </summary>
	public partial class WebForm1 : System.Web.UI.Page
	{
		
		DBClass db=new DBClass();
		private void Display_Error(string er)
		{
			string s=er.ToString().Replace("'"," ");
			s=s.Replace("\r\n"," ");
			err.Text ="<script language='javascript'> window.alert('"+s+"')</script>";
		}	
		public void DGInvoice_SortCommand(Object o, DataGridSortCommandEventArgs e) 
		{
			try
			{
				if(Session["Sort_In"].ToString().EndsWith("DESC"))
				{
					Session["Sort_In"] = e.SortExpression.ToString()+ "  ASC";
				}
				else
				{
					Session["Sort_In"] = e.SortExpression.ToString()+ "  DESC";
				}
				DGInvoice.CurrentPageIndex = 0;
				if(TxtInvoice.Text.Trim() !="")
				{
					DGInvoice.DataSource = CreateDataSource(TxtInvoice.Text.Trim());
				}
				else
				{
					DGInvoice.DataSource = CreateDataSource_Func();
				}
				DGInvoice.DataBind();
			}
			catch(Exception ex)
			{
				Display_Error(ex.Message);
			}
		}
		protected void DGInvoice_Command(Object sender,DataGridCommandEventArgs e) 
		{
			if(e.CommandName =="Print_Command")
			{
				try
				{
					//migration start backup
					//LblResult.Text="<script language='javascript'>window.open('PrintInvoice.aspx?invoice="+e.Item.Cells[2].Text.ToString()+"&customercode="+e.Item.Cells[9].Text.ToString()+"','_blank','toolbar=no,width=750,height=1000,resizable=yes,top=0,left=0')</script>";
				    //migration end backup
					//migration start update
					LblResult.Text="<script language='javascript'>window.open('PrintInvoice.aspx?invdate="+e.Item.Cells[4].Text.ToString()+"&invoice="+e.Item.Cells[2].Text.ToString()+"&customercode="+e.Item.Cells[9].Text.ToString()+"','_blank','toolbar=no,width=750,height=1000,resizable=yes,top=0,left=0')</script>";
					//migration end update
				}
				catch (Exception ex)
				{
					Display_Error(ex.Message.ToString());
				}
			}
			else if(e.CommandName =="Export_Command")
			{
				try
				{
					//migration start backup
					LblResult.Text="<script language='javascript'>window.open('PrintInvoice_xls.aspx?invoice="+e.Item.Cells[2].Text.ToString()+"','_blank','toolbar=no,width=300,height=150,resizable=yes,top=0,left=0')</script>";
				    //migration end backup
					//migration start update
					LblResult.Text="<script language='javascript'>window.open('PrintInvoice_xls.aspx?invdate="+e.Item.Cells[4].Text.ToString()+"&invoice="+e.Item.Cells[2].Text.ToString()+"&customercode="+e.Item.Cells[9].Text.ToString()+"','_blank','toolbar=no,width=300,height=150,resizable=yes,top=0,left=0')</script>";
					//migration end update
				}
				catch (Exception ex)
				{
					Display_Error(ex.Message.ToString());
				}
			}		 
		}

		protected void  DGInvoice_PageChanger(Object Source ,DataGridPageChangedEventArgs E)
		{
			try
			{
				DGInvoice.CurrentPageIndex = E.NewPageIndex ;
				if(TxtInvoice.Text.Trim() !="")
				{
					DGInvoice.DataSource = CreateDataSource(TxtInvoice.Text.Trim());
				}
				else
				{
					DGInvoice.DataSource = CreateDataSource_Func();
				}
				DGInvoice.DataBind();
			}
			catch(Exception ex)
			{
				Display_Error(ex.Message);
			}
		}
		public ICollection CreateDataSource( string invoice) 
		{
			try
			{
				DataSet ds=new DataSet();
				if(invoice.Trim() =="")
				{
					ds =db.Select_Open_Invoice();
				}
				else
				{
					ds =db.Select_Open_Invoice_ByInv(invoice.Trim());
				}
				DataTable dt = new DataTable();
				DataRow dr;
				dt.Columns.Add(new DataColumn("SalesOrder"));
				dt.Columns.Add(new DataColumn("Customer"));
				dt.Columns.Add(new DataColumn("Invoice"));
				dt.Columns.Add(new DataColumn("PoNo"));
				dt.Columns.Add(new DataColumn("InvoiceDate",System.Type.GetType("System.DateTime")));
				dt.Columns.Add(new DataColumn("Price"));
				dt.Columns.Add(new DataColumn("Phone"));
				dt.Columns.Add(new DataColumn("Fax"));
				dt.Columns.Add(new DataColumn("CustomerCode"));
				for (int i = 0; i < ds.Tables[0].Rows.Count; i++) 
				{
					dr = dt.NewRow();
					dr[0] = (string) ds.Tables[0].Rows[i]["SalesOrder"].ToString();
					dr[1] = (string) ds.Tables[0].Rows[i]["Name"].ToString();
					dr[2] = (string) ds.Tables[0].Rows[i]["OverDueInvoice"].ToString();
					dr[3] = (string) ds.Tables[0].Rows[i]["CustomerPONumber"].ToString();
					if(ds.Tables[0].Rows[i]["InvoiceDate"] !=null)
					{
						if(ds.Tables[0].Rows[i]["InvoiceDate"].ToString() !="")
						{
							dr[4] =  Convert.ToDateTime(ds.Tables[0].Rows[i]["InvoiceDate"].ToString());
						}
					}
					if(ds.Tables[0].Rows[i]["CurrencyValue"] !=null)
					{
						if(ds.Tables[0].Rows[i]["CurrencyValue"].ToString().Trim() !="")
						{
							dr[5] = String.Format("{0:$###,#0.00}", Convert.ToDecimal(ds.Tables[0].Rows[i]["CurrencyValue"].ToString()));
						}
					}
					dr[6] = (string) ds.Tables[0].Rows[i]["Telephone"].ToString();
					dr[7] = (string) ds.Tables[0].Rows[i]["Fax"].ToString();
					dr[8] = (string) ds.Tables[0].Rows[i]["Customer"].ToString();
					dt.Rows.Add(dr);
				}
				Session["save"]=dt;
				DataView dv = new DataView(dt);
				dv.Sort = (string) Session["Sort_In"];
				return dv;
			}
			catch(Exception ex)
			{
				Display_Error(ex.Message);
				return null;
			}	
		}
		public ICollection CreateDataSource_Func() 
		{
			try
			{
				if(Session["save"] !=null)
				{
					DataTable dttab=new DataTable();
					dttab=(DataTable)Session["save"];
					DataView dv = new DataView(dttab);
					dv.Sort = (string) Session["Sort_In"];
					return dv;
				}
				else
				{
					return null;
				}
			}
			catch(Exception ex)
			{
				Display_Error(ex.Message);
				return null;
			}	
		}
		protected void Page_Load(object sender, System.EventArgs e)
		{
			err.Text="";
			lblinfo.Text="";
			if(! IsPostBack)
			{
				TxtInvoice.Text="";
				Session["Sort_In"] = "InvoiceDate ASC";
				DGInvoice.CurrentPageIndex = 0;
				DGInvoice.DataSource = CreateDataSource("");
				DGInvoice.DataBind();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
		protected void LBSearch_Click(object sender, System.EventArgs e)
		{
			if(TxtInvoice.Text.Trim() !="")
			{
				try
				{
					DGInvoice.CurrentPageIndex =0;
					DGInvoice.DataSource = CreateDataSource(TxtInvoice.Text.Trim());
					DGInvoice.DataBind();
				}
				catch(Exception ex)
				{
					Display_Error(ex.Message);
				}
			}
		}
	}
}
