<%@ Page language="c#" Codebehind="ManageInvoicing.aspx.cs" AutoEventWireup="false" Inherits="AutoInvoicing.WebForm1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>WebForm1</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript">
		 function showPleaseWait()  
		{     
			document.getElementById('PleaseWait').style.display = 'block';  
		}
		</script>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" rightMargin="0" topMargin="0" bgColor="#ffe4b5">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table3" border="0" cellSpacing="1" cellPadding="1" width="100%">
				<TR>
					<TD align="center">
						<TABLE id="Table1" border="0" cellSpacing="0" cellPadding="0" width="100%" style="Z-INDEX: 0">
							<TR>
								<TD bgColor="#ffe4b5" height="25" noWrap align="center">
									<asp:label id="LblPartNo" runat="server" Font-Underline="True" Font-Size="9pt" Font-Bold="True">Open Invoices</asp:label></TD>
							</TR>
							<TR>
								<TD bgColor="#e6ebcd" noWrap align="center">
									<TABLE id="Table2" border="0" cellSpacing="1" cellPadding="1" width="300">
										<TR>
											<TD style="WIDTH: 131px" align="right">
												<asp:label style="Z-INDEX: 0" id="Label3" runat="server" Font-Size="9pt" DESIGNTIMEDRAGDROP="21">Search Invoice:</asp:label></TD>
											<TD style="WIDTH: 69px">
												<asp:textbox id="TxtInvoice" runat="server" Width="78px"></asp:textbox></TD>
											<TD>
												<asp:linkbutton style="Z-INDEX: 0" id="LBSearch" onmouseup="showPleaseWait()" runat="server" Font-Size="9pt"
													Font-Bold="True">Search</asp:linkbutton></TD>
										</TR>
									</TABLE>
									<DIV style="Z-INDEX: 0; TEXT-ALIGN: center; WIDTH: 963px; DISPLAY: none; HEIGHT: 24px; COLOR: white; VERTICAL-ALIGN: top"
										id="PleaseWait" class="helptext">
										<TABLE style="POSITION: relative; FONT-SIZE: 10pt; TOP: 0px; LEFT: 0px" id="MyTable" cellSpacing="0"
											cellPadding="0" width="200" height="15">
											<TR>
												<TD height="15"><FONT color="red">
														<MARQUEE behavior="alternate">Please Wait...</MARQUEE>
													</FONT>
												</TD>
											</TR>
										</TABLE>
									</DIV>
								</TD>
							</TR>
							<TR>
								<TD vAlign="top" noWrap align="center">
									<asp:panel style="Z-INDEX: 0" id="Panel1" runat="server">
										<asp:datagrid style="Z-INDEX: 0" id="DGInvoice" runat="server" Font-Size="8pt" Width="100%" Height="144px"
											AllowPaging="True" AllowSorting="True" OnSortCommand="DGInvoice_SortCommand" OnItemCommand="DGInvoice_Command"
											OnPageIndexChanged="DGInvoice_PageChanger" PageSize="25" AutoGenerateColumns="False" CellPadding="1"
											BorderWidth="1px" BackColor="#D9D5AE" BorderColor="White" BorderStyle="Solid" ForeColor="Gray">
											<FooterStyle ForeColor="#404040" BackColor="#F7DFB5"></FooterStyle>
											<SelectedItemStyle Font-Bold="True" ForeColor="Black" BackColor="White"></SelectedItemStyle>
											<EditItemStyle Font-Size="Smaller" HorizontalAlign="Center" ForeColor="Gray" VerticalAlign="Middle"></EditItemStyle>
											<AlternatingItemStyle BackColor="#DCE0BE"></AlternatingItemStyle>
											<ItemStyle ForeColor="#404040" BackColor="#E6EBCD"></ItemStyle>
											<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#D9D5AE"></HeaderStyle>
											<Columns>
												<asp:BoundColumn DataField="SalesOrder" SortExpression="SalesOrder" ReadOnly="True" HeaderText="S/O No.">
													<HeaderStyle HorizontalAlign="Center" Width="80px" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Customer" SortExpression="Customer" ReadOnly="True" HeaderText="Customer">
													<HeaderStyle HorizontalAlign="Center" Width="200px" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Invoice" SortExpression="Invoice" ReadOnly="True" HeaderText="Invoice">
													<HeaderStyle HorizontalAlign="Center" Width="80px" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="PoNo" SortExpression="PoNo" ReadOnly="True" HeaderText="Customer P/O">
													<HeaderStyle HorizontalAlign="Center" Width="100px" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="InvoiceDate" SortExpression="InvoiceDate" ReadOnly="True" HeaderText="Invoice Date"
													DataFormatString="{0:MM/dd/yyyy}">
													<HeaderStyle HorizontalAlign="Center" Width="70px" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Price" ReadOnly="True" HeaderText="$ Value">
													<HeaderStyle HorizontalAlign="Center" Width="70px" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Phone" HeaderText="Phone">
													<HeaderStyle HorizontalAlign="Center" Width="80px" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Fax" ReadOnly="True" HeaderText="Fax">
													<HeaderStyle HorizontalAlign="Center" Width="80px" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:BoundColumn>
												<asp:ButtonColumn Text="Invoice" HeaderText="Print" CommandName="Print_Command">
													<HeaderStyle HorizontalAlign="Center" Width="60px" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:ButtonColumn>
												<asp:BoundColumn Visible="False" DataField="CustomerCode" HeaderText="CustomerCode"></asp:BoundColumn>
												<asp:ButtonColumn Text="Export Invoice" HeaderText="Export Invoice" CommandName="Export_Command">
													<HeaderStyle Width="80px"></HeaderStyle>
												</asp:ButtonColumn>
											</Columns>
											<PagerStyle HorizontalAlign="Center" ForeColor="#404040" Mode="NumericPages"></PagerStyle>
										</asp:datagrid>
										<asp:Label style="Z-INDEX: 0" id="lblinfo" runat="server" Font-Size="9pt" ForeColor="Red"></asp:Label>
									</asp:panel>
									<asp:label style="Z-INDEX: 0" id="err" runat="server" Font-Underline="True" Font-Size="Smaller"></asp:label>
									<asp:label style="Z-INDEX: 0" id="LblResult" runat="server" DESIGNTIMEDRAGDROP="21"></asp:label></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
