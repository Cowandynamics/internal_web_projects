﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Form_16._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 300px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="style1">
            <tr>
                <td colspan="2" align="center">
    
        <asp:Label ID="Lblinfo1" runat="server" Font-Bold="True" Font-Size="11pt" 
                        Font-Underline="True" ForeColor="#000066">Search &amp; Print Form 16</asp:Label>
    
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
    
        <asp:Label ID="Lblinfo0" runat="server" Font-Bold="False" Font-Size="11pt" 
                        Font-Underline="False" ForeColor="White">*************</asp:Label>
    
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="Label1" runat="server" Text="Label">Enter Sales order:</asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TxtSalesOrder" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <asp:LinkButton ID="LBSearch" runat="server" Font-Bold="True" Font-Size="10pt" 
                        onclick="LBSearch_Click">Search &amp; Print</asp:LinkButton>
                </td>
            </tr>
        </table>
    
        <asp:Label ID="Lblinfo" runat="server"></asp:Label>
    
    </div>
    </form>
</body>
</html>
