﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net.Mail;
using System.IO;

namespace Form_16
{
    public class MailService
    {
        public static string SmtpServer = ConfigurationSettings.AppSettings["SMTPServer"];
        public static string SmtpPort = ConfigurationSettings.AppSettings["SMTPPort"];
        public static string SmtpLogin = ConfigurationSettings.AppSettings["SMTPlogin"];
        public static string SmtpPassword = ConfigurationSettings.AppSettings["SMTPPassword"];
        public static bool SmtpDebug = bool.Parse(ConfigurationSettings.AppSettings["SMTPDebug"]);
        public static string Root = ConfigurationSettings.AppSettings["QRoot"];

        private MailService()
        { }
        public static void SendMail(string from, string to, string cc, string subject, string body, string attachmentPath, bool highPriority)
        {
            MailMessage message = new MailMessage();
            message.Sender = new MailAddress("admin@cowandynamics.com", "[Cowandynamics]");
            message.From = new MailAddress("admin@cowandynamics.com", "[Cowandynamics]");
            if (!string.IsNullOrEmpty(from))
            {
                message.Headers.Add("Disposition-Notification-To", "<" + from + ">");
            }
            string invalidEmailText = "";
            string[] emails = to.IndexOf(',') > 0 ? to.Split(';') : (to.IndexOf(';') > 0 ? to.Split(';') : to.Split(' '));
            if (emails.Length > 0)
            {
                foreach (string email in emails)
                {
                    if (!string.IsNullOrEmpty(email.Trim()) && IsValidEmail(email.Trim()))
                    {
                        message.To.Add(new MailAddress(email.Trim()));
                    }
                    else
                    {
                        message.To.Add(new MailAddress("admin@cowandynamics.com"));
                        invalidEmailText = "Invoice couldn't email to the AR email specifed<br><p>Please correct the email address(Separate emails using ';' or ','):" + to + "<p><br><br>";
                    }
                }
            }
            else
            {
                message.To.Add(new MailAddress("admin@cowandynamics.com"));
            }
            if (!string.IsNullOrEmpty(cc) && IsValidEmail(cc.Trim()))
            {
                message.CC.Add(new MailAddress(cc.Trim()));
            }

            if (SmtpDebug)
            {
                message.Bcc.Add(new MailAddress("admin@cowandynamics.com"));
            }
            message.Subject = subject;
            message.Body = invalidEmailText + body;
            message.IsBodyHtml = true;

            if (highPriority)
            {
                message.Priority = MailPriority.High;
            }

            SmtpClient client = new SmtpClient(SmtpServer, int.Parse(SmtpPort));
            client.EnableSsl = true;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(SmtpLogin, SmtpPassword);

            if (!string.IsNullOrEmpty(attachmentPath))
            {
                Attachment attach = new Attachment(attachmentPath);
                message.Attachments.Add(attach);
            }
            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
                StreamWriter wrtr = new StreamWriter(Root + "/Auto Invoicing.txt", true);
                wrtr.WriteLine(DateTime.Now.ToString() + " | " + ex.Message + "/r/n");
                wrtr.Close();
            }
        }

        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}
