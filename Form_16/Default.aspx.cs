﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Collections;
using System.Data;
using System.IO;
using System.Configuration;
namespace Form_16
{
    public partial class _Default : System.Web.UI.Page
    {
        private static string QuoteSystemLocal = ConfigurationSettings.AppSettings["QuoteSystemLocal"];
        private static string QuoteSystemICylinder = ConfigurationSettings.AppSettings["QuoteSystemICylinder"];
        private static string SysproLocal = ConfigurationSettings.AppSettings["SysproLocal"];

        public static string LocalDBServer = ConfigurationSettings.AppSettings["QLocalReportServer"];
        public static string LocalDBLogin = ConfigurationSettings.AppSettings["QLocalReportLogin"];
        public static string LocalDBPassword = ConfigurationSettings.AppSettings["QLocalReportPassword"];
        public static string LocalDB = ConfigurationSettings.AppSettings["QLocalReportDatabase"];

        public static string ELocalDBServer = ConfigurationSettings.AppSettings["ELocalReportServer"];
        public static string ELocalDBLogin = ConfigurationSettings.AppSettings["ELocalReportLogin"];
        public static string ELocalDBPassword = ConfigurationSettings.AppSettings["ELocalReportPassword"];
        public static string ELocalDB = ConfigurationSettings.AppSettings["ELocalReportDatabase"];

        public void unittest()
        {
            DataSet dsTest ;
            //= Select_InvoicedNo_Today();
            //ArrayList alTest = Select_PODetails("189325");
            //ArrayList alTest = Select_OperatorandAddress("166876");
            ArrayList alTest =Select_PODetails("189325");
            string strTest = Select_PNoDescription("01-PK29H                      ");
            //alTest =Select_OperatorandAddress("166876");
            dsTest=SelectOrderDetails("166880");
            for (int i = 0; i < alTest.Count; i++)
            {
                strTest = alTest[i].ToString();
            }

            foreach (DataRow r in dsTest.Tables[0].Rows)
            {
                foreach (DataColumn c in dsTest.Tables[0].Columns)
                {
                    strTest = r[c].ToString();
                }
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            unittest();
            if (!IsPostBack)
            {
                TxtSalesOrder.Text = "";
            }
        }
        private void Display_Message(string er)
        {
            string s = er.ToString().Replace("'", " ");
            Lblinfo.Text = "<script language='javascript'> window.alert('" + s + "')</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "MyPopUpScript" + DateTime.Now.Ticks.ToString(), Lblinfo.Text.ToString(), false);
        }
        private static SqlConnection SqlCon;
        private static SqlConnection SqlCon1;
        private static SqlConnection SqlCon2;
        private static readonly object m_Lock = new object();
        public static SqlConnection GetConnection()
        {
            if (SqlCon == null || SqlCon.State == ConnectionState.Closed)
            {
                SqlCon = new SqlConnection();
                try
                {
                    SqlCon.ConnectionString = QuoteSystemLocal;
                }
                catch (Exception)
                {
                    if (SqlCon.State != ConnectionState.Closed)
                    {
                        SqlCon.Close();
                    }
                }
                lock (m_Lock)
                {
                    SqlCon.Open();
                }
            }
            return SqlCon;
        }
        public static void Close_Connection()
        {
            if (SqlCon != null && SqlCon.State == ConnectionState.Open)
                SqlCon.Close();
        }
        public static SqlConnection GetConnection_P()
        {
            if (SqlCon1 == null || SqlCon1.State == ConnectionState.Closed)
            {
                SqlCon1 = new SqlConnection();
                try
                {
                    SqlCon1.ConnectionString = SysproLocal;
                }
                catch (Exception)
                {
                    if (SqlCon1.State != ConnectionState.Closed)
                    {
                        SqlCon1.Close();
                    }
                }
                lock (m_Lock)
                {
                    SqlCon1.Open();
                }
            }
            return SqlCon1;
        }
        public static void Close_P()
        {
            if (SqlCon1 != null && SqlCon1.State == ConnectionState.Open)
                SqlCon1.Close();
        }
        public static SqlConnection GetConnection_I()
        {
            if (SqlCon2 == null || SqlCon2.State == ConnectionState.Closed)
            {
                SqlCon2 = new SqlConnection();
                try
                {
                    SqlCon2.ConnectionString = QuoteSystemICylinder;
                }
                catch (Exception)
                {
                    if (SqlCon2.State != ConnectionState.Closed)
                    {
                        SqlCon2.Close();
                    }
                }
                lock (m_Lock)
                {
                    SqlCon2.Open();
                }
            }
            return SqlCon2;
        }
        public static void Close_I()
        {
            if (SqlCon2 != null && SqlCon2.State == ConnectionState.Open)
                SqlCon2.Close();
        }

        public void RemoveFiles(string strPath)
        {
            System.IO.DirectoryInfo di = new DirectoryInfo(strPath);
            FileInfo[] fiArr = di.GetFiles();
            foreach (FileInfo fri in fiArr)
            {
                if (fri.Extension.ToString() == ".pdf")
                {
                    fri.Delete();
                }
            }
        }
        public int Insert_Form16Data(string SoNo, string PoNo, string Customer, string Address, string Contact, string QuotePerson, string RFQDate, string QuoteNo
                    , string StockCode, string OrderPerson, string OrderDate, string ADwgPerson, string CowanDwgDate, string SignDate, string ProjectManager
                    , string POPerson, string PODate, string PONumber, string ActualDate, string MainJob, string DeliveryDate, string OType, string QuoteDate, string InvoiceNo, string InvoiceDate,string Description)
        {
            string query =
              "Insert into [Form16_Report_Table] ([SoNo],[PoNo],[Customer],[Address],[Contact],[QuotePerson],[RFQDate],[QuoteNo],[StockCode],[OrderPerson] "
                + " ,[OrderDate],[ADwgPerson],[CowanDwgDate],[SignDate],[ProjectManager],[POPerson],[PODate],[PONumber],[ActualDate],[MainJob],[DeliveryDate],[OType],[QuoteDate],[InvoiceNo],[InvoiceDate],[Description]) Values "
                + " (@SoNo,@PoNo,@Customer,@Address,@Contact,@QuotePerson,@RFQDate,@QuoteNo,@StockCode,@OrderPerson,@OrderDate,@ADwgPerson,@CowanDwgDate, "
                + " @SignDate,@ProjectManager,@POPerson,@PODate,@PONumber,@ActualDate,@MainJob,@DeliveryDate,@OType,@QuoteDate,@InvoiceNo,@InvoiceDate,@Description) ";
            SqlCommand command = new SqlCommand();
            command.CommandText = query;
            command.CommandType = CommandType.Text;
            command.Parameters.AddWithValue("@SoNo", SoNo.ToString().Trim());
            command.Parameters.AddWithValue("@PoNo", PoNo.ToString().Trim());
            command.Parameters.AddWithValue("@Customer", Customer.ToString().Trim());
            command.Parameters.AddWithValue("@Address", Address.ToString().Trim());
            command.Parameters.AddWithValue("@Contact", Contact.ToString().Trim());
            command.Parameters.AddWithValue("@QuotePerson", QuotePerson.ToString().Trim());
            command.Parameters.AddWithValue("@RFQDate", RFQDate.ToString().Trim());
            command.Parameters.AddWithValue("@QuoteNo", QuoteNo.ToString().Trim());
            command.Parameters.AddWithValue("@StockCode", StockCode.ToString().Trim());
            command.Parameters.AddWithValue("@OrderPerson", OrderPerson.ToString().Trim());
            command.Parameters.AddWithValue("@OrderDate", OrderDate.ToString().Trim());
            command.Parameters.AddWithValue("@ADwgPerson", ADwgPerson.ToString().Trim());
            command.Parameters.AddWithValue("@CowanDwgDate", CowanDwgDate.ToString().Trim());
            command.Parameters.AddWithValue("@SignDate", SignDate.ToString().Trim());
            command.Parameters.AddWithValue("@ProjectManager", ProjectManager.ToString().Trim());
            command.Parameters.AddWithValue("@POPerson", POPerson.ToString().Trim());
            command.Parameters.AddWithValue("@PODate", PODate.ToString().Trim());
            command.Parameters.AddWithValue("@PONumber", PONumber.ToString().Trim());
            command.Parameters.AddWithValue("@ActualDate", ActualDate.ToString().Trim());
            command.Parameters.AddWithValue("@MainJob", MainJob.ToString().Trim());
            command.Parameters.AddWithValue("@DeliveryDate", DeliveryDate.ToString().Trim());
            command.Parameters.AddWithValue("@OType", OType.ToString().Trim());
            command.Parameters.AddWithValue("@QuoteDate", QuoteDate.ToString().Trim());
            command.Parameters.AddWithValue("@InvoiceNo", InvoiceNo.ToString().Trim());
            command.Parameters.AddWithValue("@InvoiceDate", InvoiceDate.ToString().Trim());
            command.Parameters.AddWithValue("@Description", Description.ToString().Trim());
            int str = 0;
            try
            {
                IDisposable connection = null;
                connection = GetConnection_P();
                command.Connection = (SqlConnection)connection;
                str = command.ExecuteNonQuery();
            }
            finally
            {
                Close_P();
            }
            return str;
        }
        public string Delete_Form16Data()
        {
            string query =
              "Delete From [Form16_Report_Table]";
            SqlCommand command = new SqlCommand();
            command.CommandText = query;
            command.CommandType = CommandType.Text;
            string str = "";
            try
            {
                IDisposable connection = null;
                connection = GetConnection_P();
                command.Connection = (SqlConnection)connection;
                str = command.ExecuteNonQuery().ToString();
            }
            finally
            {
                Close_P();
            }
            return str;
        }
        
        public ArrayList Select_OrderDetails(string sono, string pono, string pno)
        {
            string query =
              "SELECT distinct wl.[QuoteNo],wl.[PartNo],wl.[SONo],wl.[PONo],wl.[Customer],pc.[Contact],wl.[EntryDate],wl.[DwgDate],wl.[SignedDate] "
                + " ,wl.[DWGAssignedPerson],wl.[DeliveryDate],wl.[ShopActualDate],wl.[ShopAssignedPerson],rt.[RDate],wl.[Types],pc.[PrepairedBy],pc.[SearchDate] "
                + "  FROM [Work_Log_TableV1] as wl left join Pricing_Customer_TableV1 as pc on wl.[QuoteNo]=pc.[QuoteNo] "
                + " left join RFQ_TableV1 as rt on pc.[QuoteNo]=rt.[QuoteNo] where wl.[SONo]=@sono and wl.[PONo]=@pono and wl.[PartNo]=@pno and wl.[Revision] is null;";
            SqlCommand command = new SqlCommand();
            command.CommandText = query;
            command.CommandType = CommandType.Text;
            command.Parameters.AddWithValue("@sono", sono.Trim());
            command.Parameters.AddWithValue("@pono", pono.Trim());
            command.Parameters.AddWithValue("@pno", pno.Trim());
            ArrayList list = new ArrayList();
            try
            {
                command.Connection = GetConnection(); ;
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.Read())
                {
                    list.Add(reader.GetValue(0).ToString());
                    list.Add(reader.GetValue(1).ToString());
                    list.Add(reader.GetValue(2).ToString());
                    list.Add(reader.GetValue(3).ToString());
                    list.Add(reader.GetValue(4).ToString());
                    list.Add(reader.GetValue(5).ToString());
                    list.Add(reader.GetValue(6).ToString());
                    list.Add(reader.GetValue(7).ToString());
                    list.Add(reader.GetValue(8).ToString());
                    list.Add(reader.GetValue(9).ToString());
                    list.Add(reader.GetValue(10).ToString());
                    list.Add(reader.GetValue(11).ToString());
                    list.Add(reader.GetValue(12).ToString());
                    list.Add(reader.GetValue(13).ToString());
                    list.Add(reader.GetValue(14).ToString());
                    list.Add(reader.GetValue(15).ToString());
                    list.Add(reader.GetValue(16).ToString());
                }
                reader.Close();
            }
            finally
            {
                Close_Connection();
            }
            return list;
        }
        public ArrayList Select_PODetails(string sono)
        {
            //migration start backup
            //string query =
            //  " select top 1 * from Openquery( \"COWAN-NT-2003\", 'select wm.job,pm.PurchaseOrder ,pm.OrderEntryDate,pm.Buyer "
            //   + " from  WIPMASTER as wm  left join WIPJOBALLMAT as wj on wm.Job=wj.Job left join PORMASTERDETAIL as pd on wj.Stockcode=pd.MStockCode "
            //   + " left join PORMASTERHDR as pm on pd.PurchaseOrder=pm.PurchaseOrder "
            //   + " where  wm.JobDescription=\"" + sono.Trim() + "\" and  pd.LineType=\"1\" and pm.PurchaseOrder <>\"\" order by pm.OrderEntryDate DESC') where  PurchaseOrder is not null;";
            //migration end backup
            //migration start update
            string query =
               " select top 1 * from Openquery( \"lnkSYSPRONEW\", 'select wm.Job,pm.PurchaseOrder ,pm.OrderEntryDate,pm.Buyer "
                + "  from  WipMaster as wm  left join WipJobAllMat as wj on wm.Job=wj.Job left join PorMasterDetail as pd on wj.StockCode=pd.MStockCode "
                + "  left join PorMasterHdr as pm on pd.PurchaseOrder=pm.PurchaseOrder "
                + "  where  wm.JobDescription=''" + sono.Trim() + "'' and  pd.LineType=''1'' and pm.PurchaseOrder <>'''' order by pm.OrderEntryDate DESC') where  PurchaseOrder is not null;";
            //migration end update
            SqlCommand command = new SqlCommand();
            command.CommandText = query;
            command.CommandType = CommandType.Text;
            command.Parameters.AddWithValue("@sono", sono.Trim());
            ArrayList list = new ArrayList();
            try
            {
                command.Connection = GetConnection_P(); ;
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.Read())
                {
                    list.Add(reader.GetValue(0).ToString());
                    list.Add(reader.GetValue(1).ToString());
                    list.Add(reader.GetValue(2).ToString());
                    list.Add(reader.GetValue(3).ToString());
                }
                reader.Close();
            }
            finally
            {
                Close_P();
            }
            return list;
        }
        public string Select_PNoDescription(string pno)
        {
            //migraion start backup
            //string query =
            //  " declare @parm10 as nvarchar(50);"
            //   + " set @parm10=@pno;"
            //   +" declare @MyString    varchar(max);"
            //   +" set @MyString = ('SELECT Description, LongDesc FROM INVMASTER WHERE StockCode='''+@parm10+'''');"
            //   +" set @MyString =  N'select * from  openquery(\"COWAN-NT-2003\", ''' + REPLACE(@MyString, '''', '''''') + ''')';"
            //   +" EXEC (@MyString);";
            //migration end backup
            //migration start update
            string query =
              " declare @parm10 as nvarchar(50); "
               +"  set @parm10=@pno; "
               +"  declare @MyString    varchar(max); "
               +"  set @MyString = ('SELECT Description, LongDesc FROM InvMaster WHERE StockCode='''+@parm10+''''); "
               + "  set @MyString =  N'select * from  openquery(\"lnkSYSPRONEW\", ''' + REPLACE(@MyString, '''', '''''') + ''')'; "
               +"  EXEC (@MyString);";
            //migration end update
            SqlCommand command = new SqlCommand();
            command.CommandText = query;
            command.CommandType = CommandType.Text;
            command.Parameters.AddWithValue("@pno", pno.Trim().Replace("\"",""));
            string str = "";
            try
            {
                command.Connection = GetConnection_P(); ;
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.Read())
                {
                    str=(reader.GetValue(0).ToString()+" "+reader.GetValue(1).ToString());
                }
                reader.Close();
            }
            finally
            {
                Close_P();
            }
            return str;
        }
        public ArrayList Select_OperatorandAddress(string sono)
        {
            //migration start backup
            //string query =
            //  "select * from Openquery( \"COWAN-NT-2003\", 'select LastOperator,ShipAddress1,ShipAddress2,ShipAddress3,ShipPostalCode from SORMASTER  where SalesOrder=\"" + sono.Trim() + "\"');";
            //migration end backup
            //migration start update
            string query =
              "select * from Openquery( \"lnkSYSPRONEW\", 'select LastOperator,ShipAddress1,ShipAddress2,ShipAddress3,ShipPostalCode from SorMaster  where SalesOrder=''" + sono.Trim() + "''');";
            //migration end update
            SqlCommand command = new SqlCommand();
            command.CommandText = query;
            command.CommandType = CommandType.Text;
            command.Parameters.AddWithValue("@sono", sono.Trim());
            ArrayList list = new ArrayList();
            try
            {
                command.Connection = GetConnection_P(); ;
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.Read())
                {
                    list.Add(reader.GetValue(0).ToString());
                    list.Add(reader.GetValue(1).ToString() + "' " + reader.GetValue(2).ToString() + " " + reader.GetValue(3).ToString() + " " + reader.GetValue(4).ToString());
                }
                reader.Close();
            }
            finally
            {
                Close_P();
            }
            return list;
        }
        public ArrayList Select_QuoteDetails_ICyl(string qno, string table)
        {
            string query =
              "SELECT [Contact],[SearchDate],[PrepairedBy] FROM " + table + " Where [QuoteNo] =@qno;";
            SqlCommand command = new SqlCommand();
            command.CommandText = query;
            command.CommandType = CommandType.Text;
            command.Parameters.AddWithValue("@qno", qno.Trim());
            ArrayList list = new ArrayList();
            try
            {
                command.Connection = GetConnection_I(); ;
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.Read())
                {
                    list.Add(reader.GetValue(0).ToString());
                    list.Add(reader.GetValue(1).ToString());
                    list.Add(reader.GetValue(2).ToString());
                }
                reader.Close();
            }
            finally
            {
                Close_I();
            }
            return list;
        }
        public DataSet SelectOrderDetails(string sono)
        {
            SqlCommand command = new SqlCommand();
            command.CommandType = CommandType.Text;
            //migration start backup
            //command.CommandText = "select SalesOrder,CustomerPONumber,LastInvoice,MStockCode from Openquery( \"COWAN-NT-2003\", 'select distinct sm.SalesOrder,sm.CustomerPONumber,sm.LastInvoice,sd.MStockCode from SORMASTER as sm  "
            //                + " left join SORDETAIL as sd on sm.SalesOrder=sd.SalesOrder where sm.SalesOrder=\"" + sono.Trim() + "\" and sd.LineType=\"1\" order by sm.SalesOrder ASC')";
            //migration end backup
            //migration start update
            command.CommandText = "select SalesOrder,CustomerPONumber,LastInvoice,MStockCode from Openquery( \"lnkSYSPRONEW\", 'select distinct sm.SalesOrder,sm.CustomerPoNumber,sm.LastInvoice,sd.MStockCode from SorMaster as sm  "
                            +" left join SorDetail as sd on sm.SalesOrder=sd.SalesOrder where sm.SalesOrder=''" + sono.Trim() + "'' and sd.LineType=''1'' order by sm.SalesOrder ASC')";
            //migration end update
            DataSet ds = new DataSet();
            try
            {
                command.Connection = GetConnection_P();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = command;
                adapter.Fill(ds);
            }
            finally
            {
                Close_P();
            }
            return ds;
        }
        public int InsertForm16Data(string salesorder)
        {
            int result = 0;
            try
            {
                Delete_Form16Data();
                DataSet ds = new DataSet();
                ds = SelectOrderDetails(salesorder.Trim());
                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        string SoNo = "";
                        string PoNo = "";
                        string Customer = "";
                        string Address = "";
                        string Contact = "";
                        string QuotePerson = "";
                        string RFQDate = "";
                        string QuoteNo = "";
                        string StockCode = "";
                        string OrderPerson = "";
                        string OrderDate = "";
                        string ADwgPerson = "";
                        string CowanDwgDate = "";
                        string SignDate = "";
                        string ProjectManager = "";
                        string POPerson = "";
                        string PODate = "";
                        string PONumber = "";
                        string ActualDate = "";
                        string MainJob = "";
                        string DeliveryDate = "";
                        string OType = "";
                        string QuoteDate = "";
                        string InvoiceNo = "";
                        string InvoiceDate = "";
                        string Description = "";
                        InvoiceNo = ds.Tables[0].Rows[i]["LastInvoice"].ToString().Trim();
                        InvoiceDate = DateTime.Today.ToShortDateString();
                        SoNo = ds.Tables[0].Rows[i]["SalesOrder"].ToString().Trim();
                        PoNo = ds.Tables[0].Rows[i]["CustomerPONumber"].ToString().Trim();
                        StockCode = ds.Tables[0].Rows[i]["MStockCode"].ToString().Trim();
                        ArrayList orderlist = new ArrayList();
                        orderlist = Select_OrderDetails(ds.Tables[0].Rows[i]["SalesOrder"].ToString().Trim(), ds.Tables[0].Rows[i]["CustomerPONumber"].ToString().Trim(), ds.Tables[0].Rows[i]["MStockCode"].ToString().Trim());
                        if (orderlist.Count > 0)
                        {
                            QuoteNo = orderlist[0].ToString().Trim();
                            Customer = orderlist[4].ToString().Trim();
                            Contact = orderlist[5].ToString().Trim();
                            OrderDate = orderlist[6].ToString().Trim();
                            CowanDwgDate = orderlist[7].ToString().Trim();
                            SignDate = orderlist[8].ToString().Trim();
                            ADwgPerson = orderlist[9].ToString().Trim();
                            DeliveryDate = orderlist[10].ToString().Trim();
                            ActualDate = orderlist[11].ToString().Trim();
                            ProjectManager = orderlist[12].ToString().Trim();
                            RFQDate = orderlist[13].ToString().Trim();
                            OType = orderlist[14].ToString().Trim();
                            QuotePerson = orderlist[15].ToString().Trim();
                            QuoteDate = orderlist[16].ToString().Trim();
                        }
                        if (QuoteNo.Trim() != "")
                        {
                            if (QuoteNo.Substring(0, 1) == "Q")
                            {
                                ArrayList qlist = new ArrayList();
                                qlist = Select_QuoteDetails_ICyl(QuoteNo.Trim(), "[Pricing_Customer_Table]");
                                if (qlist.Count > 0)
                                {
                                    Contact = qlist[0].ToString().Trim();
                                    QuoteDate = qlist[1].ToString().Trim();
                                    QuotePerson = "I-Cylinder";
                                }
                            }
                            else if (QuoteNo.Substring(0, 1) == "Z")
                            {
                                ArrayList qlist = new ArrayList();
                                qlist = Select_QuoteDetails_ICyl(QuoteNo.Trim(), "[WEB_Pricing_Customer_TableV1]");
                                if (qlist.Count > 0)
                                {
                                    Contact = qlist[0].ToString().Trim();
                                    QuoteDate = qlist[1].ToString().Trim();
                                    QuotePerson = "I-Cylinder";
                                }
                            }
                        }
                        ArrayList polist = new ArrayList();
                        polist = Select_PODetails(ds.Tables[0].Rows[i]["SalesOrder"].ToString().Trim());
                        if (polist.Count > 0)
                        {
                            MainJob = polist[0].ToString().Trim();
                            PONumber = polist[1].ToString().Trim();
                            PODate = polist[2].ToString().Trim();
                            if (polist[3].ToString().Trim().ToUpper() == "LAYAL")
                            {
                                POPerson = "Josée Villandré";
                            }
                            else if (polist[3].ToString().Trim().ToUpper() == "JV")
                            {
                                POPerson = "Josée Villandré";
                            }
                            else
                            {
                                POPerson = polist[3].ToString().Trim();
                            }
                        }
                        ArrayList addrlist = new ArrayList();
                        addrlist = Select_OperatorandAddress(ds.Tables[0].Rows[i]["SalesOrder"].ToString().Trim());
                        if (addrlist.Count > 0)
                        {
                            if (addrlist[0].ToString().Trim().ToUpper() == "LAYAL")
                            {
                                OrderPerson = "Josée Villandré";
                            }
                            else if (addrlist[0].ToString().Trim().ToUpper() == "JV")
                            {
                                POPerson = "Josée Villandré";
                            }
                            else if (addrlist[0].ToString().Trim().ToUpper() == "DAN")
                            {
                                POPerson = "Danny Cotton";
                            }
                            else if (addrlist[0].ToString().Trim().ToUpper() == "DG")
                            {
                                POPerson = "Daniel Gagné";
                            }
                            else
                            {
                                OrderPerson = addrlist[0].ToString().Trim();
                            }
                            Address = addrlist[1].ToString().Trim();
                        }
                        Description = Select_PNoDescription(StockCode.ToString().Trim());
                        result += Insert_Form16Data(SoNo.ToString().Trim(), PoNo.ToString().Trim(), Customer.ToString().Trim(), Address.ToString().Trim(), Contact.ToString().Trim(),
                            QuotePerson.ToString().Trim(), RFQDate.ToString().Trim(), QuoteNo.ToString().Trim(), StockCode.ToString().Trim(), OrderPerson.ToString().Trim(),
                            OrderDate.ToString().Trim(), ADwgPerson.ToString().Trim(), CowanDwgDate.ToString().Trim(), SignDate.ToString().Trim(), ProjectManager.ToString().Trim(),
                            POPerson.ToString().Trim(), PODate.ToString().Trim(), PONumber.ToString().Trim(), ActualDate.ToString().Trim(), MainJob.ToString().Trim(),
                            DeliveryDate.ToString().Trim(), OType.ToString().Trim(), QuoteDate.ToString().Trim(), InvoiceNo.ToString().Trim(), InvoiceDate.ToString().Trim(),Description.ToString().Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                MailService.SendMail(
                        "admin@cowandynamics.com",
                        "admin@cowandynamics.com",
                        "",
                        DateTime.Now.ToString() + " Form16 Report error ",
                        ex.Message.ToString(),
                        "",
                        true
                        );	               
            }
            return result;
        }

        protected void LBSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (TxtSalesOrder.Text.Trim() != "")
                {
                    int result = 0;
                    result = InsertForm16Data(TxtSalesOrder.Text.Trim());
                    if (result > 0)
                    {
                        CrystalDecisions.CrystalReports.Engine.ReportDocument objReport = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
                        string path;
                        path = Request.PhysicalApplicationPath;
                        RemoveFiles(path + "/email/");
                        objReport.Load(path + "/crtfiles/Form16Report.rpt");
                        objReport.Refresh();
                        objReport.SetDatabaseLogon(ELocalDBLogin, ELocalDBPassword, ELocalDBServer, ELocalDB);
                        CrystalDecisions.Shared.DiskFileDestinationOptions diskOpts = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                        objReport.ExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                        Random RD = new Random();
                        string rdnum = "";
                        rdnum = RD.Next().ToString();
                        diskOpts.DiskFileName = path + "/email/Form16Report_" + rdnum + ".pdf";
                        objReport.ExportOptions.DestinationOptions = diskOpts;
                        objReport.Export();
                        Lblinfo.Text = "<script language='javascript'>window.open('/email/Form16Report_" + rdnum + ".pdf','_blank','toolbar=no,width=800,height=700,resizable=yes,top=0,left=0')</script>";
                    }
                }
            }
            catch (Exception ex)
            {
                Display_Message(ex.Message);
            }
        }
    }
}
